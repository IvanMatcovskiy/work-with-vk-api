import vk_api
import vk
import time
import sys
import matplotlib.pyplot as plt
import requests
def main():
    login, password = 'login', 'password'
    vk_session = vk_api.VkApi(login, password)
    vk_session.authorization()
    api = vk_session.get_api()

    string = 'hse'

    response1 = api.groups.search(q = string, sort = 0, count = 1000)
    
    dusers = {}
    
    session = vk.Session()
    api = vk.API(session)

    for elem in response1['items']:
        id_gr = elem['screen_name']
        print(id_gr)
        if id_gr[:4] == 'club' and id_gr[4:].isdigit():
            id_gr = id_gr[4:]
        shift = 0    
        while True :
            try:
                response2 = api.groups.getMembers (group_id = id_gr,  offset = shift, count = 900)
                for id_user in response2['users']:
                    dusers[id_user] = 0
                print(len(dusers))
                shift += len(response2['users'])
                if shift == response2['count']:
                    break
            except vk.exceptions.VkAPIError:
                print('private community')
                break
            except requests.exceptions.ReadTimeout:
                print('requests.exceptions.ReadTimeout')
            except requests.exceptions.ConnectionError:
                print('requests.exceptions.ConnectionError')
            except requests.exceptions.ChunkedEncodingError:
                print('requests.exceptions.ChunkedEncodingError')
    lusers = dusers.keys()

    number_iteration = 48

    for it in range(number_iteration):
        start = 0
        while True:
            try:
                end = start + 900
                if start + 900 > len(lusers):
                    end = len(lusers)
            
                response3 = api.users.get(user_ids = lusers[start : end], fields = 'last_seen')
                for elem in response3:
                    if 'deactivated' in elem.keys() or (time.time() - elem['last_seen']['time']) > 2592000:
                        dusers[elem['uid']] = -1
                        continue
                    if (time.time() - elem['last_seen']['time']) < 1800:
                        dusers[elem['uid']] += 1 

                start += 900
                if start > len(lusers):
                    break
            except requests.exceptions.ReadTimeout:
                print('requests.exceptions.ReadTimeout')
            except requests.exceptions.ConnectionError:
                print('requests.exceptions.ConnectionError')
            except requests.exceptions.ChunkedEncodingError:
                print('requests.exceptions.ChunkedEncodingError')

        if it < number_iteration - 1:
            print('sleep 1800')
            time.sleep(1800)
    val = dusers.values()
    data = {i : val.count(i) for i in range(-1, number_iteration + 1)}
    
    f = open('hse_data.txt', 'w')
    for i in range(-1, number_iteration + 1):
        f.write(str(i) + ' : ' + str(data[i]) + ', ')
    f.close()
    
    print(data)
    plt.figure(figsize=(8,8))

    summ = sum(data.values())
    fracs = [(float(i)/summ)*100 for i in data.values()]
    
    plt.pie(fracs, labels=data.keys(), autopct='%1.1f%%', startangle=90)
    plt.title('Activity', bbox={'facecolor':'0.8', 'pad':5})
    plt.show()
main()
