import vk
import time
import matplotlib.pyplot as plt
import requests
def main():
    session = vk.Session()
    api = vk.API(session)

    gr_id = 'leroy_merlin'  # id of group

    dsex = {}

    dage = {}
    withage = 0
    
    dcity = {}
    
    duniver = {}
    univer = 0

    dead = 0
    alive = 0
    total = 0

    shift = 0    
    while True :
        try:
            response1 = api.groups.getMembers (group_id = gr_id,  offset = shift, count = 900)
            total = response1['count']
            users = response1['users']
            response2 = api.users.get(user_ids = users, fields = 'sex, city, last_seen, bdate, education')
            for elem in response2:
                if 'deactivated' in elem.keys() or (time.time() - elem['last_seen']['time']) > 2592000:
                    dead += 1 
                    continue
                alive += 1 
                if not dsex.has_key(elem['sex']):
                    dsex[elem['sex']] = 0
                dsex[elem['sex']] = dsex[elem['sex']] + 1

                city = elem['city']
                if city > 1:
                    city = 2
                if not dcity.has_key(city):
                    dcity[city] = 0
                dcity[city] = dcity[city] + 1
            
                if 'bdate' in elem.keys():
                    bdate = elem['bdate'].split('.')
                    if len(bdate) > 2:
                        age = 2016 - int(bdate[2]) #rough calculation
                        if not dage.has_key(age): 
                            dage[age] = 0
                        dage[age] = dage[age] + 1
                        withage = withage + 1
                
                if 'university' in elem.keys():
                    if not duniver.has_key(elem['university']):
                        duniver[elem['university']] = 0
                    duniver[elem['university']] = duniver[elem['university']] + 1
                    univer = univer + 1

            shift += len(response1['users'])
            if shift == total:
                break
        except requests.exceptions.ReadTimeout:
                print('requests.exceptions.ReadTimeout')
        except requests.exceptions.ConnectionError:
                print('requests.exceptions.ConnectionError')
        except requests.exceptions.ChunkedEncodingError:
                print('requests.exceptions.ChunkedEncodingError')
    print('Total')
    print(total)
    print('Dead')
    print(dead)
    print('Alive')
    print(alive)
    print('Sex')
    print(dsex) # 0-none, 1-female, 2-male 
    print('City')
    print(dcity)# 0-none, 1-Moscow, 2-other
    print('Age')
    print(dage)
    print("WithAge")
    print(withage)
    print('WithoutAge')
    print(alive - withage)
    print('University')
    univer = univer - duniver[0]
    del duniver[0]
    print(duniver)

    print('withUniversity')
    print(univer)
    print('WithoutUniversity')
    print(alive - univer)
    plt.bar(range(len(dsex)), dsex.values(), align='center')
    plt.xticks(range(len(dsex)), dsex.keys())
    plt.title('Sex')
    plt.xlabel('0-none, 1-female, 2-male')
    plt.show()

    plt.bar(range(len(dcity)), dcity.values(), align='center')
    plt.xticks(range(len(dcity)), dcity.keys())
    plt.xlabel('0-none, 1-Moscow, 2-other')
    plt.title('City')
    plt.show()

    plt.bar(range(len(dage)), dage.values(), align='center')
    plt.xticks(range(len(dage)), dage.keys(), size='xx-small')
    #plt.xlim(0, 10000)
    plt.title('Age')
    plt.xlabel('have age ' + str(withage) + ', have not age ' + str(alive - withage))
    plt.show()
    
    plt.bar(range(len(duniver)), duniver.values(), align='center')
    plt.xticks(range(len(duniver)), duniver.keys(), size='xx-small')
    plt.title('University')
    plt.xlabel('have university ' + str(univer) + ', have not university ' + str(alive - univer))
    plt.show()
main()