
import networkx as net

g = net.read_gpickle("MPSU_community.gpickle")

print('Full community')
print('nodes', g.number_of_nodes())
print('edges', g.number_of_edges())
print('density',net.density(g))
print('clustering_coefficient', net.average_clustering(g))
redundancy = ((float)(g.number_of_edges())/(g.number_of_nodes() - 1)) - 1
print('redundancy', redundancy)
print('number_components', net.number_connected_components(g))

print('The most largest connected component')
lcc = max(net.connected_component_subgraphs(g), key=len)
print('nodes', lcc.number_of_nodes())
print('edges', lcc.number_of_edges())
print('density',net.density(lcc))
print('clustering_coefficient', net.average_clustering(lcc))
redundancy = ((float)(lcc.number_of_edges())/(lcc.number_of_nodes() - 1)) - 1
print('redundancy', redundancy)
print('average_path_length', net.average_shortest_path_length(lcc))
print('diameter', net.diameter(lcc))