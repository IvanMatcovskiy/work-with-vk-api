import matplotlib.pyplot as plt

# make a square figure and axes

data = {0: 115, 1: 1, 2: 2, -1: 26}
plt.figure(figsize=(8,8))

summ = sum(data.values())
print(summ)
fracs = [(float(i)/summ)*100 for i in data.values()]
print(fracs)
plt.pie(fracs, labels=data.keys(), autopct='%1.1f%%', startangle=90)
plt.title('Activity', bbox={'facecolor':'0.8', 'pad':5})
plt.show()