# -*- coding: utf-8 -*-

import vk_api
import vk
import sys
import requests
import time
import networkx as net
def main():
    vk_session = vk_api.VkApi('login', 'password')
    vk_session.authorization()
    api = vk_session.get_api()

    string = 'hse'

    response1 = api.groups.search(q = string, sort = 0, count = 1000)
    susers = set()
    
    session = vk.Session()
    api = vk.API(session)

    dead = 0

    for elem in response1['items']:
        id_gr = elem['screen_name']
        print(id_gr)
        if id_gr[:4] == 'club' and id_gr[4:].isdigit():
            id_gr = id_gr[4:]
        shift = 0    
        while True :
            try:
                response2 = api.groups.getMembers (group_id = id_gr,  offset = shift, count = 900)
                users = response2['users']
                response4 = api.users.get(user_ids = users, fields = 'last_seen')
                for elem in response4:
                    if 'deactivated' in elem.keys() or (time.time() - elem['last_seen']['time']) > 2592000:
                        dead += 1
                    else:
                        susers.add(elem['uid'])
                print(len(susers))
                shift += len(response2['users'])
                if shift == response2['count']:
                    break
            except vk.exceptions.VkAPIError:
                print('private community')
                break
            except requests.exceptions.ReadTimeout:
                print('requests.exceptions.ReadTimeout')
            except requests.exceptions.ConnectionError:
                print('requests.exceptions.ConnectionError')
            except requests.exceptions.ChunkedEncodingError:
                print('requests.exceptions.ChunkedEncodingError')
    g = net.Graph()
    lusers = list(susers)
    ever = len(lusers)
    i = 0
    while True:
        try:
            g.add_node(lusers[i])
            response3 = api.friends.get(user_id = lusers[i])
            for friend in response3:
                if friend in susers:
                    g.add_edge(lusers[i], friend)
            print(lusers[i])
            print((i + 1), ever)
            i += 1
            if i == ever:
                break
        except vk.exceptions.VkAPIError:
            print(lusers[i])
            print((i + 1), ever)
            print('User was deactivated')
            i += 1
            if i == ever:
                break
        except requests.exceptions.ReadTimeout:
            print('requests.exceptions.ReadTimeout')
        except requests.exceptions.ConnectionError:
            print('requests.exceptions.ConnectionError')
        except requests.exceptions.ChunkedEncodingError:
            print('requests.exceptions.ChunkedEncodingError')
    net.write_gpickle(g, "HSE_community.gpickle")
    print('dead', dead)
main()